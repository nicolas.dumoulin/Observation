package fr.cemagref.observation.gui;


/**
 * <code>Configurable</code> This interface is implemented by GUI element that are configurables. (element in a list, panel, ...)
 */
public interface Configurable {
    
    /**
     * <code>configure</code> is called when the user want to configure the component.
     *
     */
    public void configure();
}
