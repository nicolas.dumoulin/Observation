package fr.cemagref.observation.gui;

import java.awt.event.ActionListener;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class ControlsFactory {
    
    public static JButton makeButton(String imageName,
            ActionListener listener,
            String toolTipText,
            String altText) {
        // Look for the image.
        String imgLocation = "toolbarButtonGraphics/"
            + imageName
            + ".gif";
        URL imageURL = ControlsFactory.class.getClassLoader().getResource(imgLocation);
        // Create and initialize the button.
        JButton button = new JButton();
        button.setToolTipText(toolTipText);
        button.addActionListener(listener);
        if (imageURL != null) {
            //image found
            button.setIcon(new ImageIcon(imageURL, altText));
        } else {
            //no image found
            button.setText(altText);
            System.err.println("Resource not found: " + imgLocation);
        }
        
        return button;
    }
    
}
