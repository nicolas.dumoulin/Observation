package fr.cemagref.observation.gui;

import fr.cemagref.observation.kernel.ObservableManager;
import fr.cemagref.observation.kernel.ObservableManager.ClassAndObserver;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

@SuppressWarnings("serial")
public class ObserversManagerPanel extends JPanel implements ListSelectionListener {

    private ObserversManagerHandler observersManagerHandler;
    private JList list;
    private JButton removeButton;
    private JButton confButton;
    private JButton showButton;
   
    private HashSet<ClassAndObserver> observersShowed = new HashSet<ClassAndObserver>();
    
    public ObserversManagerPanel(ObserversManagerHandler observersManagerHandler) {
        super();
        this.observersManagerHandler = observersManagerHandler;
        
        this.setLayout(new BorderLayout());
        // The list
        list = new JList(ObservableManager.getObservableManager());
        list.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        list.setLayoutOrientation(JList.HORIZONTAL_WRAP);
        list.addListSelectionListener(this);
        JScrollPane listScroller = new JScrollPane(list);
        this.add(listScroller,BorderLayout.CENTER);
        
        // The toolbar
        JToolBar toolBar = new JToolBar();
        toolBar.setFloatable(false);
        add(toolBar,BorderLayout.PAGE_END);

        // The "remove" button
        removeButton = ControlsFactory.makeButton("general/Delete24",new RemoveButtonListener(),"Remove the selected module","Remove");        
        toolBar.add(removeButton);
        
        // The "conf" button
        confButton = ControlsFactory.makeButton("general/Edit24",new ConfButtonListener(),"Configure the selected module","Configure");        
        toolBar.add(confButton);

        // The "show" button
        showButton = ControlsFactory.makeButton("general/Find24",new ShowButtonListener(),"Show the selected module","Show");        
        toolBar.add(showButton);

        // set buttons enabled or not
        this.valueChanged(null);
    }

    public void valueChanged(ListSelectionEvent e) {
        if (list.getSelectedIndex()>=0) {
            ClassAndObserver classAndObserver = (ClassAndObserver)list.getSelectedValue();
            removeButton.setEnabled(true);
            confButton.setEnabled(classAndObserver.getObserver() instanceof Configurable);
            showButton.setEnabled(
                    // observers must be drawable
                    classAndObserver.getObserver() instanceof Drawable
                    // and observers must not be yet displayed
                    && !observersShowed.contains(classAndObserver)
            );
        } else {
            removeButton.setEnabled(false);
            confButton.setEnabled(false);
            showButton.setEnabled(false);
        }
        
    }

    private class RemoveButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (JOptionPane.showConfirmDialog(observersManagerHandler.getHandlingFrame(),"Sure ?","Confirmation",JOptionPane.YES_NO_OPTION)==0) {
                ClassAndObserver item = (ClassAndObserver)list.getSelectedValue();
                ObservableManager.removeObserverListener(item.getType(),item.getObserver());
            }
        }
    }

    private class ConfButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ((Configurable)((ClassAndObserver)list.getSelectedValue()).getObserver()).configure();
        }
    }
    
    private class ShowButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            ClassAndObserver classAndObserver = (ClassAndObserver)list.getSelectedValue();
            if (observersShowed.add(classAndObserver)) {
                // force buttons enabling refresh
                valueChanged(null);
                // get display and add it in handler
                observersManagerHandler.showDrawable((Drawable)classAndObserver.getObserver());
            }
        }
    }
}
