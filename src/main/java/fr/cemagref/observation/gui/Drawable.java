package fr.cemagref.observation.gui;

import javax.swing.JComponent;

/**
 * <code>Drawable</code> A component implements this interface if it could be graphically drawed as a JComponent.
 */
public interface Drawable {
    
    /**
     * <code>getTitle</code>
     *
     * @return The title of this component.
     */
    public String getTitle();
    
    public JComponent getDisplay();
}
