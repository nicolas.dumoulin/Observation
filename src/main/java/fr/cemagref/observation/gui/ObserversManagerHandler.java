package fr.cemagref.observation.gui;

import javax.swing.JFrame;

/**
 * <code>ObserversManagerHandler</code> is those who handles the ObservableManager.
 */
public interface ObserversManagerHandler {
    /**
     * <code>getHandlingFrame</code> is used by dialog box to know which is the parent frame. 
     */
    public JFrame getHandlingFrame();

    public void showDrawable(Drawable drawable);
}
