package fr.cemagref.observation.observers;

import fr.cemagref.observation.kernel.ObservablesHandler;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Observer that write data in XML format as defined by the Mexico working group.
 */
public class XMLObserver extends ConsoleObserver {

  private static class ObservableDataInfos {

    String tagOpeningWithName;
    String tagOpening;
    String tagClosing;
    int numberOfDimensions;
  }
  private static transient Map<Class, String> typeForXml;
  private transient List<ObservableDataInfos> observableDataInfosList;

  public XMLObserver(boolean sysout, String outputFile) {
    super(sysout, outputFile);
  }

  @Override
  public void init() {
    super.init();
    if (typeForXml == null) {
      typeForXml = new HashMap<Class, String>();
      typeForXml.put(Double.class, "d");
      typeForXml.put(double.class, "d");
      typeForXml.put(Integer.class, "i");
      typeForXml.put(int.class, "i");
      typeForXml.put(String.class, "s");
    }
    if (observableDataInfosList == null) {
      observableDataInfosList = new ArrayList<ObservableDataInfos>();
    }
    outputStream.print("<outputData>\n");
  }

  @Override
  public void addObservable(ObservablesHandler classObservable) {
    if (typeForXml == null) {
      init();
    }
    if (observableDataInfosList == null) {
      init();
    }
    // Storing the infos for this observable
    ObservableDataInfos infos;
    for (int i = 0; i < classObservable.numberOfAttributes(); i++) {
      infos = new ObservableDataInfos();
      ObservablesHandler.ObservableFetcher fetcher = classObservable.getObservableFetcher(i);
      Class observableType = fetcher.getDeclaredType();
      // if the type is an array, going in depth to find the final component type and the number of dimensions
      infos.numberOfDimensions = 0;
      while (observableType.isArray()) {
        infos.numberOfDimensions++;
        observableType = observableType.getComponentType();
      }
      infos.tagOpening = "<" + typeForXml.get(observableType) + ">";
      infos.tagOpeningWithName = "<" + typeForXml.get(observableType) + " name =\"";
      infos.tagClosing = "</" + typeForXml.get(observableType) + ">";
      observableDataInfosList.add(infos);
    }
  }

  @Override
  public void valueChanged(ObservablesHandler clObservable, Object instance, long t) {
    for (int i = 0; i < clObservable.numberOfAttributes(); i++) {
      try {
        //System.out.println("XMLObserver::valueChanged" + clObservable.getObservableFetcher(i).fetchValue(instance) + clObservable.getDeclaredName(i) + clObservable.getDescription(i));
        outputStream.print("<step time =\"");
        outputStream.print(t);
        outputStream.println("\">");
        printData(observableDataInfosList.get(i), clObservable.getObservableFetcher(i).fetchValue(instance), clObservable.getDeclaredName(i),
                clObservable.getDescription(i));
        outputStream.println("</step>");
      } catch (IllegalArgumentException e) {
        e.printStackTrace();
      } catch (IllegalAccessException e) {
        e.printStackTrace();
      } catch (InvocationTargetException e) {
        e.printStackTrace();
        return;
      }
    }
  }

  private void printData(ObservableDataInfos infos, Object data, String declaredName, String description) {
    if (infos.numberOfDimensions > 0) {
      // Open the matrix tag
      outputStream.print("<m name=\"");
      outputStream.print(description);
      outputStream.print("\">\n");
      // print metadata for each dimensions
      Object dimension = data;
      int dimensionExtent;
      for (int i = 1; i <= infos.numberOfDimensions; i++) {
        outputStream.print("<dimension name=\"dim");
        outputStream.print(i);
        outputStream.print("\" size=\"");
        dimensionExtent = Array.getLength(dimension);
        outputStream.print(dimensionExtent);
        outputStream.println("\"><label></label></dimension>");
        if (dimensionExtent > 0) {
          dimension = Array.get(dimension, 0);
        } else {
          outputStream.println("</m>");
          return;
        }
      }
      // print the matrix data
      for (int i = 0; i < Array.getLength(data); i++) {
        printMatrixData(infos, Array.get(data, i), declaredName, description, 0);
      }
      // close the matrix tag
      outputStream.println("</m>");
    } else {
      printScalarData(infos, data, description);
    }
  }

  private void printMatrixData(ObservableDataInfos infos, Object data, String declaredName, String description, int depth) {
    if (data.getClass().isArray()) {
      for (int i = 0; i < Array.getLength(data); i++) {
        printMatrixData(infos, Array.get(data, i), declaredName, description, depth + 1);
      }
    } else {
      printScalarData(infos, data);
    }
  }

  private void printScalarData(ObservableDataInfos infos, Object data, String description) {
    outputStream.print(infos.tagOpeningWithName);
    outputStream.print(description);
    outputStream.print("\">");
    outputStream.print(data);
    outputStream.println(infos.tagClosing);
  }

  private void printScalarData(ObservableDataInfos infos, Object data) {
    outputStream.print(infos.tagOpening);
    outputStream.print(data);
    outputStream.println(infos.tagClosing);
  }

  @Override
  public void close() {
    outputStream.print("</outputData>");
    super.close();
  }
}
