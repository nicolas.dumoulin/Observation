package fr.cemagref.observation.observers;

import java.lang.reflect.InvocationTargetException;

import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.ohoui.annotations.Description;

public class CSVObserver extends ConsoleObserver {

    @Description(name="Field separator", tooltip="")
    private char separator = ';';
    
    /** <code>classObservable</code> is used to display name of attributes as header */
    private transient ObservablesHandler classObservable;

    public CSVObserver(boolean sysout, String outputFile) {
        super(sysout, outputFile);
    }
    
    @Override
    public void addObservable(ObservablesHandler classObservable) {
        ObservablesHandler bak = this.classObservable;
        this.classObservable = classObservable;
        if (bak != null) // The observable has been changed 
            this.init();
    }

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long t) {
        StringBuffer buf = new StringBuffer();
        StringBuffer sbSeparator = new StringBuffer(" "+this.separator+" ");
        // print current Time
        buf.append(t);
        // print value of each field
        for (int i =0;i<clObservable.numberOfAttributes();i++) {
            buf.append(sbSeparator);
            try {
                buf.append(clObservable.getObservableFetcher(i).fetchValue(instance));
            } catch (IllegalArgumentException e) {
                buf.append("N/A");
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                buf.append("N/A");
                e.printStackTrace();
            } catch (InvocationTargetException e) {
    			e.printStackTrace();
                return;
            }
        }
        outputStream.println(buf);
    }

    @Override
    public void init() {
        super.init();
        if (classObservable != null) {
            // Headers printing
            StringBuffer buf = new StringBuffer();
            StringBuffer sbSeparator = new StringBuffer(" "+this.separator+" ");
            buf.append("Time");
            for (int i =0;i<classObservable.numberOfAttributes();i++) {
                buf.append(sbSeparator);
                buf.append(classObservable.getDescription(i));
            }
            outputStream.println(buf);
        }
    }
    
    
}
