package fr.cemagref.observation.observers;

import java.awt.Dimension;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.lang.reflect.InvocationTargetException;

import fr.cemagref.observation.gui.Configurable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.ohoui.annotations.Anchor;
import fr.cemagref.ohoui.annotations.Description;
import fr.cemagref.ohoui.annotations.Link;
import fr.cemagref.ohoui.filters.NoTransientField;
import fr.cemagref.ohoui.swing.OhOUI;
import fr.cemagref.ohoui.swing.OhOUIDialog;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;

public class ConsoleObserver implements ObserverListener, Configurable {

    @Description(name="Use standard output",tooltip = "")
    @Link(action="disable",target="filename")
    private boolean sysout = true;
    
    @Anchor(id="filename")
    private File outputFile = new File("");
    
    private transient File outputFileBak = outputFile;
    protected transient PrintStream outputStream = System.out;

    public ConsoleObserver(boolean sysout, String outputFile) {
        this.sysout = sysout;
        this.outputFile = new File(outputFile);
        this.init();
    }
    
    @Override
    public void addObservable(ObservablesHandler classObservable) {
        // nothing to do
    }

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long t) {
        StringBuffer buf = new StringBuffer(t+" "+instance.getClass().getName() + " :\n");
        for (int i =0;i<clObservable.numberOfAttributes();i++) {
            buf.append("  "+clObservable.getDescription(i)+" = ");
            try {
                buf.append(clObservable.getObservableFetcher(i).fetchValue(instance));
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
    			e.printStackTrace();
                return;
            }
            if (i<clObservable.numberOfAttributes()-1) buf.append("\n");
        }
        outputStream.println(buf);
    }

    protected void println(CharSequence message) {
        if (sysout) {
            outputStream.print(this.getClass().getSimpleName()+" :: ");
        }
        outputStream.println(message);
    }
        
    @Override
    public void configure() {
        OhOUIDialog dialog = OhOUI.getDialog(null,this,new NoTransientField());
        dialog.setSize(new Dimension(510, 160));
        dialog.setVisible(true);
        if (!outputFile.equals(outputFileBak)) init();
        outputFileBak = outputFile;
    }

    @Override
    public void init() {
        if (sysout) {
            outputStream = System.out;
        } else {
            try {
                // it's rather better to use a buffetred outputstream, and to disable to default buffer of the PrintStream
                outputStream = new PrintStream(new BufferedOutputStream(new FileOutputStream(outputFile), 1024), false);
            } catch (FileNotFoundException e) {
            	// TODO bring info to the user. Or prevent exception when directory of file doesn't exist
                e.printStackTrace();
                outputStream = System.out;
            }
        }    
    }

    @Override
    public void close() {
        if (outputStream != System.out)
            outputStream.close();
    }

}
