package fr.cemagref.observation.observers;

import fr.cemagref.observation.kernel.ObservablesHandler;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * Observer that write data in XML format as defined by the Mexico working group.
 */
public class XMLObserverSelectable extends ConsoleObserver {

    protected String observableVariable;
    protected transient ObservablesHandler.ObservableFetcher variable;
    protected transient String tagOpeningWithName;
    protected transient String tagOpening;
    protected transient String tagClosing;
    protected transient int numberOfDimensions;
    private static transient Map<Class, String> typeForXml;

    public XMLObserverSelectable(boolean sysout, String outputFile) {
        super(sysout, outputFile);
    }

    @Override
    public void init() {
        super.init();
        if (typeForXml == null) {
            typeForXml = new HashMap<Class, String>();
            typeForXml.put(Double.class, "d");
            typeForXml.put(double.class, "d");
            typeForXml.put(Integer.class, "i");
            typeForXml.put(int.class, "i");
            typeForXml.put(String.class, "s");
        }
    }

    @Override
    public void addObservable(ObservablesHandler classObservable) {
        if (typeForXml == null) {
            init();
        }
        // Storing the infos for this observable
        variable = classObservable.getObservableFetcherByName(observableVariable);
        if (variable != null) {
            Class observableType = variable.getDeclaredType();
            // if the type is an array, going in depth to find the final component type and the number of dimensions
            numberOfDimensions = 0;
            while (observableType.isArray()) {
                numberOfDimensions++;
                observableType = observableType.getComponentType();
            }
            tagOpening = "<" + typeForXml.get(observableType) + ">";
            tagOpeningWithName = "<" + typeForXml.get(observableType) + " name =\"";
            tagClosing = "</" + typeForXml.get(observableType) + ">";
        }
    }

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long t) {
        if (variable != null) {
            try {
                outputStream.print("<step time =\"");
                outputStream.print(t);
                outputStream.println("\">");
                printData(variable.fetchValue(instance), variable.getDeclaredName(),
                        variable.getDescription());
                outputStream.println("</step>");
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    private void printData(Object data, String declaredName, String description) {
        if (numberOfDimensions > 0) {
            // Open the matrix tag
            outputStream.print("<m name=\"");
            outputStream.print(description);
            outputStream.print("\">\n");
            // print metadata for each dimensions
            Object dimension = data;
            int dimensionExtent;
            for (int i = 1; i <= numberOfDimensions; i++) {
                outputStream.print("<dimension name=\"dim");
                outputStream.print(i);
                outputStream.print("\" size=\"");
                dimensionExtent = Array.getLength(dimension);
                outputStream.print(dimensionExtent);
                outputStream.println("\"><label></label></dimension>");
                if (dimensionExtent > 0) {
                    dimension = Array.get(dimension, 0);
                } else {
                    outputStream.println("</m>");
                    return;
                }
            }
            // print the matrix data
            for (int i = 0; i < Array.getLength(data); i++) {
                printMatrixData(Array.get(data, i), declaredName, description, 0);
            }
            // close the matrix tag
            outputStream.println("</m>");
        } else {
            printScalarData(data, description);
        }
    }

    private void printMatrixData(Object data, String declaredName, String description, int depth) {
        if (data.getClass().isArray()) {
            for (int i = 0; i < Array.getLength(data); i++) {
                printMatrixData(Array.get(data, i), declaredName, description, depth + 1);
            }
        } else {
            printScalarData(data);
        }
    }

    private void printScalarData(Object data, String description) {
        outputStream.print(tagOpeningWithName);
        outputStream.print(description);
        outputStream.print("\">");
        outputStream.print(data);
        outputStream.println(tagClosing);
    }

    private void printScalarData(Object data) {
        outputStream.print(tagOpening);
        outputStream.print(data);
        outputStream.println(tagClosing);
    }
}
