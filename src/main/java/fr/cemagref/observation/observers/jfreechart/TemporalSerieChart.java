package fr.cemagref.observation.observers.jfreechart;

import java.lang.reflect.InvocationTargetException;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import fr.cemagref.observation.kernel.ObservablesHandler;

public class TemporalSerieChart extends TemporalChart {

    public TemporalSerieChart() {
        super();
        init();
    }
    
    public TemporalSerieChart(ObservablesHandler.ObservableFetcher variable) {
        super(variable);
        init();
    }

    /**
     * @see fr.cemagref.observation.kernel.ObserverListener#valueChanged(fr.cemagref.observation.kernel.ObservablesHandler, java.lang.Object, int)
     */
    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long t) {
    	// we fetch the value of the observable
        double value;
        try {
        	Object objectValue = variable.fetchValue(instance);
        	if (objectValue instanceof Integer)
        		value = (Integer)objectValue;
        	else
        		value = (Double)objectValue;
        } catch (IllegalArgumentException e1) {
            e1.printStackTrace();
            return;
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
            return;
        } catch (InvocationTargetException e) {
			e.printStackTrace();
            return;
        }

        int index = dataset.indexOf(instance.hashCode());
        XYSeries serie;
        if (index >= 0) {
            serie = dataset.getSeries(index);
        } else {
            // This is the first value of the observable 
            serie = new XYSeries(instance.hashCode());
            dataset.addSeries(serie);
        }
        serie.add(t, value);
    }

    /**
     * @see fr.cemagref.observation.kernel.ObserverListener#init()
     */
    @Override
    public void init() {
        super.init();
        dataset = new XYSeriesCollection();
        graphTypeUpdated();
    }

    /**
     * @see fr.cemagref.observation.kernel.ObserverListener#close()
     */
    @Override
    public void close() {
    }

}
