package fr.cemagref.observation.observers.jfreechart;

public enum GraphType {
	
	POINT(1,"Points"),
	LINE(2,"Lignes"),
	AREA(3,"Aires"),
	AREASTEP(4,"Aires en escalier"),
	STEP(5,"Escalier");
	
	private int code;
	private String description;
	
	private GraphType(int i,String description) {
		code=i;
		this.description=description;
	}
	
	public int code() {
		return code;
	}
	
	public String getDescription() {
		return description;
	}
	
	public String toString() {
		return getDescription();
	}
}
