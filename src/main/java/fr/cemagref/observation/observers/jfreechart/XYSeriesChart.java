package fr.cemagref.observation.observers.jfreechart;

import com.thoughtworks.xstream.XStream;
import javax.swing.JComboBox;
import javax.swing.JComponent;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeriesCollection;

import fr.cemagref.observation.gui.Configurable;
import fr.cemagref.observation.gui.Drawable;
import fr.cemagref.observation.kernel.ObservablesHandler;
import fr.cemagref.observation.kernel.ObservablesHandler.ObservableFetcher;
import fr.cemagref.observation.kernel.ObserverListener;
import fr.cemagref.ohoui.filters.NoTransientField;
import fr.cemagref.ohoui.swing.OUIPanel;
import fr.cemagref.ohoui.swing.OhOUI;
import fr.cemagref.ohoui.swing.OhOUIDialog;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import org.jfree.data.xy.XYDataset;

public class XYSeriesChart implements ObserverListener, Configurable, Drawable {

    private transient ChartPanel chartPanel;
    private transient JFreeChart jfchart;
    protected transient XYDataset dataset;
    /**
     * <code>variable</code> is the variable to represent
     */
    protected transient List<ObservableFetcher> variableXY;
    protected transient ObservablesHandler classObservable;
    protected transient List<Integer> xIndex = null;
    protected transient List<Integer> yIndex = null;
    protected GraphType graphType = GraphType.LINE;
    private String title = "",  xAxisLabel = "x",  yAxisLabel = "y";

    public XYSeriesChart() {
        graphTypeUpdated();
    }

    public XYSeriesChart(List<ObservableFetcher> xy) {
        this();
        this.variableXY = xy;
        variableUpdated();
    }

    protected void variableUpdated() {
        if ((xIndex != null) && (xIndex.size() > 0)) {
            xAxisLabel = variableXY.get(xIndex.get(0)).getDescription();
        }
        if ((yIndex != null) && (yIndex.size() > 0)) {
            yAxisLabel = variableXY.get(yIndex.get(1)).getDescription();
        }
        title = "Observation of " + yAxisLabel;
    }

    protected void graphTypeUpdated() {
        PlotOrientation orientation = PlotOrientation.VERTICAL;
        boolean legend = true;
        boolean tooltips = true;
        boolean urls = false;

        if (graphType.equals(GraphType.POINT)) {
            jfchart = ChartFactory.createScatterPlot(title, xAxisLabel, yAxisLabel, dataset, orientation, legend, tooltips, urls);
        } else if (graphType.equals(GraphType.LINE)) {
            jfchart = ChartFactory.createXYLineChart(title, xAxisLabel, yAxisLabel, dataset, orientation, legend, tooltips, urls);
        } else if (graphType.equals(GraphType.AREA)) {
            jfchart = ChartFactory.createXYAreaChart(title, xAxisLabel, yAxisLabel, dataset, orientation, legend, tooltips, urls);
        } else if (graphType.equals(GraphType.STEP)) {
            jfchart = ChartFactory.createXYStepChart(title, xAxisLabel, yAxisLabel, dataset, orientation, legend, tooltips, urls);
        } else if (graphType.equals(GraphType.AREASTEP)) {
            jfchart = ChartFactory.createXYStepAreaChart(title, xAxisLabel, yAxisLabel, dataset, orientation, legend, tooltips, urls);
        }
        if (chartPanel == null) {
            chartPanel = new ChartPanel(jfchart);
        } else {
            chartPanel.setChart(jfchart);
        }
    }

    public JFreeChart getJfchart() {
        return jfchart;
    }

    @Override
    public void configure() {
        List<ObservableFetcher> variableBak = variableXY;
        OhOUIDialog dialog = OhOUI.getDialog(null, this, new NoTransientField());
// panel for X variable
        JComboBox comboBoxX = new JComboBox(classObservable.getDescriptions());
        if (variableXY != null) {
            comboBoxX.setSelectedItem(variableXY.get(0).getDescription());
        }
        OUIPanel ouiPanelX = OUIPanel.makeLabelComponentOUIPanel(variableXY, comboBoxX, "Variable for X axis", "");
        dialog.getContentPane().add(ouiPanelX.getPanel(), 0);

// panel for Y variable
        JComboBox comboBoxY = new JComboBox(classObservable.getDescriptions());
        if (variableXY != null) {
            comboBoxY.setSelectedItem(variableXY.get(1).getDescription());
        }
        OUIPanel ouiPanelY = OUIPanel.makeLabelComponentOUIPanel(variableXY, comboBoxY, "Variable for Y axis", "");
        dialog.getContentPane().add(ouiPanelY.getPanel(), 1);

        dialog.pack();
        dialog.setVisible(true);
        variableXY = new ArrayList<ObservableFetcher>();
        variableXY.add(classObservable.getObservableFetcher((String) comboBoxX.getSelectedItem()));
        variableXY.add(classObservable.getObservableFetcher((String) comboBoxY.getSelectedItem()));
        if (variableXY != null) {
            if (!variableXY.equals(variableBak)) {
                variableUpdated();
            }
        }
        graphTypeUpdated();
    }

    /**
     * @see fr.cemagref.observation.kernel.ObserverListener#addObservable(fr.cemagref.observation.kernel.ObservablesHandler)
     */
    @Override
    public void addObservable(ObservablesHandler classObservable) {
        this.classObservable = classObservable;
    }

    /**
     * @see fr.cemagref.observation.gui.Drawable#getDisplay()
     */
    @Override
    public JComponent getDisplay() {
        return chartPanel;
    }

    /**
     * @see fr.cemagref.observation.gui.Drawable#getTitle()
     */
    @Override
    public String getTitle() {
        return title;
    }

    /**
     * @see fr.cemagref.observation.kernel.ObserverListener#init()
     */
    @Override
    public void init() {
        if (classObservable != null) {
            if (classObservable.numberOfAttributes() > 0) {
                variableXY = new ArrayList<ObservableFetcher>();

                variableXY.add(classObservable.getObservableFetcher(0));
                variableXY.add(classObservable.getObservableFetcher(1));
            }
        }
        // TODO classObservable.getAttributes().length *must* be > 0
        dataset = new XYSeriesCollection();
        graphTypeUpdated();
    }

    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long t) {
        // we fetch the value of the observable
        List<Object> value = new ArrayList<Object>();
        try {
            for (ObservableFetcher va : variableXY) {
                Object objectValue = va.fetchValue(instance);
                if (objectValue instanceof Integer) {
                    value.add((Integer) objectValue);
                } else {
                    value.add((Double) objectValue);
                }
            }
        } catch (IllegalArgumentException e1) {
            e1.printStackTrace();
            return;
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
            return;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return;
        }

    /* TODO a revoir
    int index = dataset.indexOf(instance.hashCode());
    XYSeries serie;
    if (index >= 0) {
    serie = dataset.getSeries(index);
    } else {
    // This is the first value of the observable 
    serie = new XYSeries(instance.hashCode());
    dataset.addSeries(serie);
    }
    serie.add(t, value);
     * */
    }

    @Override
    public void close() {
    }

    public static void main(String[] args) {
        System.out.println(new XStream().toXML(new XYSeriesChart()));
    }
}
