package fr.cemagref.observation.observers.jfreechart;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import fr.cemagref.observation.kernel.ObservablesHandler;

/**
 * <code>TemporalMeanChart</code> allow to draw a chart representing the mean
 * of several temporal series
 */
public class TemporalMeanChart extends TemporalChart {

    /**
     * <code>serie</code> store the temporal values of the mean, ready to draw
     */
    private transient XYSeries serie;

    /**
     * <code>temporalValues</code>
     */
    private transient Map<Long, List<Double>> temporalValues = new HashMap<Long, List<Double>>();

    /**
     * This constructor init the chart with a line drawstyle.
     * 
     * @param variable
     *            the variable to represent
     */
    public TemporalMeanChart(ObservablesHandler.ObservableFetcher variable) {
        super(variable);
        init();
    }
    
    /**
     * <code>mean</code> computes the mean of the value of the list passed as
     * arguments
     */
    private double mean(List<Double> list) {
        double mean = 0;
        for (Double d : list) {
            mean += d;
        }
        mean = mean / list.size();
        return mean;
    }

    /**
     * @see fr.cemagref.observation.kernel.ObserverListener#valueChanged(fr.cemagref.observation.kernel.ObservablesHandler,
     *      java.lang.Object, int)
     */
    @Override
    public void valueChanged(ObservablesHandler clObservable, Object instance, long t) {
        // we fetch the value of the observable
        double value;
        try {
            value = (Double)variable.fetchValue(instance);
        } catch (IllegalArgumentException e1) {
            e1.printStackTrace();
            return;
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
            return;
        } catch (InvocationTargetException e) {
			e.printStackTrace();
            return;
		}

        int index = serie.indexOf(t);
        if (index < 0) {
            // This is the first value for this time step
            List<Double> list = new ArrayList<Double>();
            list.add(value);
            temporalValues.put(t, list);
            // For instance, the mean is this first value
            serie.add(t, value);
        } else {
            // we add this new value to the list
            List<Double> list = temporalValues.get(t);
            list.add(value);
            // and we update the mean
            serie.getDataItem(index).setY(mean(list));
        }
    }

    /**
     * @see fr.cemagref.observation.kernel.ObserverListener#init()
     */
    @Override
    public void init() {
        super.init();
        serie = new XYSeries("values");
        dataset = new XYSeriesCollection(serie);
        graphTypeUpdated();
    }

    /**
     * @see fr.cemagref.observation.kernel.ObserverListener#close()
     */
    public void close() {
    }

}
