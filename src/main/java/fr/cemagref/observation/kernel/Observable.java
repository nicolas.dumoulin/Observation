/**
 *  L'interface "annotation" pour marquer les observables.
 */
package fr.cemagref.observation.kernel;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <code>Observable</code> The annotation used to declare a field as observable
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.METHOD})
public @interface Observable {
    /**
     * <code>description</code> is the textual description of the observable.
     *
     * @return the textual description of the observable.
     */
    String description();
}
