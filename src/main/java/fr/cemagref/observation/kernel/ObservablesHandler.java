package fr.cemagref.observation.kernel;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

// TODO troduce in english
public class ObservablesHandler {

    private java.util.ArrayList<ObserverListener> observers;
    private transient ObservableFetcher[] fetchers;

    /**
     * Constructeur.
     * 
     * @param cl La classe à laquelle on associe le présent ClassObservable
     */
    public ObservablesHandler(Class cl) {
        // observable attributes initialisation
        List<Field> v = new ArrayList<Field>();
        List<Method> v2 = new ArrayList<Method>();

        // observable methods initialisation
        Class inheritanceNavigator = cl;
        while (inheritanceNavigator != null) {
            for (Field f : inheritanceNavigator.getDeclaredFields()) {
                if (f.isAnnotationPresent(Observable.class)) {
                    v.add(f);
                }
            }
            for (Method m : inheritanceNavigator.getDeclaredMethods()) {
                if (m.isAnnotationPresent(Observable.class)) {
                    v2.add(m);
                }
            }
            inheritanceNavigator = inheritanceNavigator.getSuperclass();
        }

        fetchers = new ObservableFetcher[v.size() + v2.size()];
        for (int i = 0; i < v.size(); i++) {
            fetchers[i] = new FieldFetcher(v.get(i));
        }
        for (int i = 0; i < v2.size(); i++) {
            fetchers[v.size() + i] = new MethodFetcher(v2.get(i));
        }

        // observers intialisation
        observers = new ArrayList<ObserverListener>();
    }

    public Object getValue(int i, Object instance) throws IllegalArgumentException, IllegalAccessException,
            InvocationTargetException {
        return fetchers[i].fetchValue(instance);
    }

    /** 
     * Equivalent à getAttributes()[n], sauf s'il n'y a aucun attribut annoté comme observable (dans
     * quel cas null est renvoyé).
     * 
     * @param n Un indice
     * @return Le java.lang.reflect.Field correspondant au n-ième attribut, ou null si aucun attribut 
     * n'est annoté comme observable.
     */
    public ObservableFetcher getObservableFetcher(int n) {
        return fetchers[n];
    }

    public ObservableFetcher[] getObservableFetchers() {
        return fetchers;
    }

    /** Renvoie l'attribut dont la description est passée en argument.
     * 
     * PERFS : Très mauvaises. A n'utiliser que pour un stockage initial de l'attribut
     * afin de l'utiliser intensivement ensuite.
     * 
     * @param desc Une description
     * @return L'attribut qui correspond à cette description.
     */
    public ObservableFetcher getObservableFetcher(String desc) {
        if ((fetchers.length == 0) || (desc == null)) {
            return null;
        }
        desc = desc.trim();
        for (int i = 0; i < fetchers.length; i++) {
            if (desc.equals(fetchers[i].getDescription().trim())) {
                return fetchers[i];
            }
        }
        return null;
    }

    public ObservableFetcher getObservableFetcherByName(String name) {
        if ((fetchers.length == 0) || (name == null)) {
            return null;
        }
        name = name.trim();
        for (int i = 0; i < fetchers.length; i++) {
            if (name.equals(fetchers[i].getDeclaredName().trim())) {
                return fetchers[i];
            }
        }
        return null;
    }

    /**
     * @return Le nombre d'attributes observables.
     */
    public int numberOfAttributes() {
        return fetchers.length;
    }

    /**
     * Accesseur vers les descriptions des attributes observables, dans le même ordre
     * que getAttributes, c'est à dire que getAttributes()[n] ou getAttribute(n) ont la description
     * getDescriptions()[n].
     * 
     * Si aucun attribut n'est annoté comme étant observable, renvoie null.
     * 
     *  PERFS : Comme les description ne sont pas destinées à être utilisées de façon intensive,
     *  le tableau est reconstruit à chaque appel de la méthode.
     * 
     * @return Un tableau de descriptions des attributes annotés comme observables. 
     * null si aucun attribut n'est annoté comme observable. 
     */
    public String[] getDescriptions() {
        String[] t = new String[fetchers.length];
        for (int i = 0; i < fetchers.length; i++) {
            t[i] = fetchers[i].getDescription();
        }
        return t;
    }

    /** Equivalent à getAttribute(n).getAnnotation(Observable.class).description() sauf si
     * aucun attribut n'est annoté comme étant observabel, dans quel cas on renvoie null.
     * @param n
     * @return La description du n-ième attribut.
     */
    public String getDescription(int n) {
        return fetchers[n].getDescription();
    }

    public String getDeclaredName(int n) {
        return fetchers[n].getDeclaredName();
    }

    /** Ajoute un ObserverListener à la liste.
     * 
     * @param ob L'ObserverListener à ajouter.
     */
    public ObserverListener addObserverListener(ObserverListener ob) {
        observers.add(ob);
        ob.addObservable(this);
        return ob;
    }

    /** Retire un seul observerListener de la liste. Renvoie true s'il a effectivement été trouvé
     * et retiré.
     * @param ob L'ObserverListener à retirer. 
     * @return
     */
    public boolean removeObserverListener(ObserverListener ob) {
        boolean response = observers.remove(ob);
        ObservableManager.getObservableManager().fireObservableManagerStructureChanged();
        return response;
    }

    /**
     * <code>size</code>
     *
     * @see java.util.ArrayList#size()
     */
    public int getObserversCount() {
        return observers.size();
    }

    /**
     * Notifie les changement aux observers déclarés. Pour le moment les appelle l'un après l'autre.
     * 
     * @param instanceEnCours
     * @param t
     */
    public void fireChanges(Object instanceEnCours, long t) {
        for (ObserverListener listener : observers) {
            listener.valueChanged(this, instanceEnCours, t);
        }
    }

    public Collection<ObserverListener> getObservers() {
        return observers;
    }

    /**
     * @see java.util.ArrayList#clear()
     */
    void clearObservers() {
        this.observers.clear();
    }

    /**
     * @see java.util.ArrayList#get(int)
     */
    public ObserverListener getObserver(int i) {
        return observers.get(i);
    }

    public abstract class ObservableFetcher {

        public ObservableFetcher(AccessibleObject accessibleObject) {
            accessibleObject.setAccessible(true);
        }

        public abstract Object fetchValue(Object instance) throws IllegalArgumentException, IllegalAccessException,
                InvocationTargetException;

        public abstract String getDeclaredName();

        public abstract String getDescription();

        public abstract Class getDeclaredType();
    }

    public class MethodFetcher extends ObservableFetcher {

        private Method method;

        public MethodFetcher(Method method) {
            super(method);
            this.method = method;
        }

        @Override
        public Object fetchValue(Object instance) throws IllegalArgumentException, IllegalAccessException,
                InvocationTargetException {
            // TODO add the ability to use arguments
            return method.invoke(instance, new Object[]{});
        }

        @Override
        public String getDescription() {
            return method.getAnnotation(Observable.class).description();
        }

        @Override
        public String getDeclaredName() {
            return method.getName();
        }

        @Override
        public Class getDeclaredType() {
            return method.getReturnType();
        }
    }

    public class FieldFetcher extends ObservableFetcher {

        private Field field;

        public FieldFetcher(Field field) {
            super(field);
            this.field = field;
        }

        @Override
        public Object fetchValue(Object instance) throws IllegalArgumentException, IllegalAccessException,
                InvocationTargetException {
            return field.get(instance);
        }

        @Override
        public String getDescription() {
            return field.getAnnotation(Observable.class).description();
        }

        @Override
        public String getDeclaredName() {
            return field.getName();
        }

        @Override
        public Class getDeclaredType() {
            return field.getType();
        }
    }
}
