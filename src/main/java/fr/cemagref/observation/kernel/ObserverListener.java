package fr.cemagref.observation.kernel;

public interface ObserverListener {
    /**
     * <code>addObservable</code> is called when an observable is connected to
     * this observable. It gives the possibility to make some init tasks.
     * 
     * @param classObservable
     *            The new observable connected to this observer
     */
    public void addObservable(ObservablesHandler classObservable);

    /**
     * <code>valueChanged</code> is invoked when the observable has updated
     * its value. It's time for the observer to do its job.
     * 
     * @param clObservable
     *            the observable handler
     * @param instance
     *            the observable object
     * @param t
     *            the current time
     */
    public void valueChanged(ObservablesHandler clObservable, Object instance, long t);

    /**
     * <code>init</code> Called at begin of simulation
     */
    public void init();

    /**
     * <code>close</code> Called at end of simulation
     * 
     */
    public void close();
}
