package fr.cemagref.observation.kernel;

import java.io.Reader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * TODO traduce in english
 */
public class ObservableManager implements ListModel {

    private static ObservableManager om = null;
    private Map<Class, ObservablesHandler> observables;
    private List<ListDataListener> listDataListeners = new ArrayList<ListDataListener>();

    /** Le constructeur. Il est private, car l'accès à l'observable manager doit
     * se faire exclusivement via la méthode statique getObservableManager().
     */
    private ObservableManager() {
		observables = new Hashtable<Class,ObservablesHandler>();
    }

    /**
     * The true mean to access Observable manager.
     *
     * @return L'observable manager du système.
     */
    public static ObservableManager getObservableManager() {
        return om == null ? om = new ObservableManager() : om;
    }

    /**
     * clear method clears all class-observers associations
     */
    public static void clear() {
        for (ObservablesHandler co : om.observables.values()) {
            co.clearObservers();
        }
        om.fireObservableManagerStructureChanged();
    }

    /**
     * Déclare une classe comme étant observable.
     *
     * Si une classe a déjà été déclarée comme observable,
     * l'appel de cette méthode est équvalent à un getObservable(Class).
     *
     * @param cl La classe déclarée comme observable
     * @return Le ClassObservable de cl.
     */
    public static ObservablesHandler addObservable(Class cl) {
        if (getObservableManager().observables.containsKey(cl)) {
            return getObservableManager().observables.get(cl);
        }
        ObservablesHandler co = new ObservablesHandler(cl);
        getObservableManager().observables.put(cl, co);
        getObservableManager().fireObservableManagerStructureChanged();
        return co;
    }

    /** Permet d'obtenir l'instance de ClassObservable associée à la classe
     * donnée en argument.
     *
     * PERFS : Les classObservable sont stockées dans une Hashtable,
     * avec une clé sur Class, c'est à dire que l'algo est de l'ordre
     * de N log(N) (où N est le nombre d'observables), avec
     * (ce qui n'est pas négligeable), une opération de base qui est Class.equals(Object).
     * En conclusion : pour une utilisation très intensive, on gardera une variable
     * pointant sur le ClassObservable, pour éviter d'appeler trop souvent cette méthode.
     *
     * @param cl La classe dont on veut le ClassObservable
     * @return Le classObservable, ou null si la classe donnée en argument
     * n'a pas été déclarée comme étant observable.
     */
    public static ObservablesHandler getObservable(Class cl) {
        return getObservableManager().observables.get(cl);
    }

    /** Ajoute un ObserverListener à la liste.
     * 
     * @param ob L'ObserverListener à ajouter.
     */
    public static ObserverListener addObserverListener(Class cl, ObserverListener ob) {
        ObserverListener observerListener = addObservable(cl).addObserverListener(ob);
        getObservableManager().fireObservableManagerStructureChanged();
        return observerListener;
    }

    /** Retire un seul observerListener de la liste. Renvoie true s'il a effectivement été trouvé
     * et retiré.
     * @param ob L'ObserverListener à retirer. 
     * @return
     */
    public static boolean removeObserverListener(Class cl, ObserverListener ob) {
        boolean response = getObservable(cl).removeObserverListener(ob);
        getObservableManager().fireObservableManagerStructureChanged();
        return response;
    }

    public static void initObservers() {
        for (ObservablesHandler classObservable : getObservableManager().observables.values()) {
            for (ObserverListener observer : classObservable.getObservers()) {
                observer.init();
            }
        }
    }

    public static void closeObservers() {
        for (ObservablesHandler classObservable : getObservables().values()) {
            for (ObserverListener observer : classObservable.getObservers()) {
                observer.close();
            }
        }
    }

    public static Map<Class, ObservablesHandler> getObservables() {
        return getObservableManager().observables;
    }

    public static void setObservers(Reader observablesReader, XStream xstream) {
        Map<Class,ObservablesHandler> observablesReaded = (Map<Class,ObservablesHandler>) xstream.fromXML(observablesReader);
        for (Map.Entry<Class,ObservablesHandler> entry : observablesReaded.entrySet()) {
            // add to the list of observers those readed from the file
            ObservablesHandler clObservable = addObservable(entry.getKey());
            for (ObserverListener observer : entry.getValue().getObservers()) {
                // use addObserverListener method garanties safe init
                clObservable.addObserverListener(observer);
            }
        }
    }

    public static void setObservers(Reader observablesReader) {
        setObservers(observablesReader, new XStream(new DomDriver()));
    }

    /**
     * @see javax.swing.ListModel#getSize()
     */
    public int getSize() {
        int size = 0;
        for (ObservablesHandler classObservable : observables.values()) {
            size += classObservable.getObserversCount();
        }
        return size;
    }

    /**
     * @see javax.swing.ListModel#getElementAt(int)
     */
    public Object getElementAt(int index) {
        Set<Map.Entry<Class, ObservablesHandler>> entrySet = observables.entrySet();
        Iterator<Map.Entry<Class, ObservablesHandler>> iterator = entrySet.iterator();
        Map.Entry<Class, ObservablesHandler> current = iterator.next();
        int count = current.getValue().getObserversCount();
        while (count <= 0) {
            current = iterator.next();
            count = current.getValue().getObserversCount();
        }
        while (count <= index) {
            current = iterator.next();
            count += current.getValue().getObserversCount();
        }
        int realIndex = index - (count - current.getValue().getObserversCount());
        assert realIndex < current.getValue().getObserversCount() : realIndex + " !< " + current.getValue().getObserversCount();
        return new ClassAndObserver(current.getKey(), current.getValue().getObserver(realIndex));
    }

    /**
     * @see javax.swing.ListModel#addListDataListener(javax.swing.event.ListDataListener)
     */
    public void addListDataListener(ListDataListener l) {
        listDataListeners.add(l);
    }

    /**
     * @see javax.swing.ListModel#removeListDataListener(javax.swing.event.ListDataListener)
     */
    public void removeListDataListener(ListDataListener l) {
        listDataListeners.remove(l);
    }

    void fireObservableManagerStructureChanged() {
        for (ListDataListener listDataListener : listDataListeners) {
            listDataListener.contentsChanged(new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, getSize()));
        }
    }

    public static class ClassAndObserver {

        private Class type;
        private ObserverListener observer;

        public ClassAndObserver(Class type, ObserverListener observer) {
            this.type = type;
            this.observer = observer;
        }

        public Class getType() {
            return type;
        }

        public ObserverListener getObserver() {
            return observer;
        }

        public String toString() {
            return observer.getClass().getSimpleName() + " -> " + type.getSimpleName();
        }

        /**
         * @see java.lang.Object#equals(java.lang.Object)
         */
        @Override
        public boolean equals(Object obj) {
            return obj.hashCode() == this.hashCode();
        }

        /**
         * @see java.lang.Object#hashCode()
         */
        @Override
        public int hashCode() {
            return type.hashCode() + observer.hashCode();
        }
    }
}
